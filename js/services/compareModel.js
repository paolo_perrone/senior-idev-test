/**
 * Created by paolo on 06/01/15.
 */

compare.factory('compareModel', function(){
    /**
     * Dataset
     */
    var data = [
        {
            "id": 1,
            "title": "Acer Aspire E1-572",
            "price": 399.00,
            "operatingSystem": "Windows 8.1",
            "dimensions": "380 x 27 x 255",
            "weight": 2.193,
            "transportWeight": 2.47,
            "processor": "Intel Core i5-4200U",
            "processorSpeed": 1.6,
            "ram": 4,
            "typeOfStorage": "Spinning HD 5400",
            "storageCapacity": 500
        },
        {
            "id": 2,
            "title": "Acer Aspire S3-392G",
            "price": 899.99,
            "operatingSystem": "Windows 8.1",
            "dimensions": "221 x 17 x 124",
            "weight": 1.618,
            "transportWeight": 1.97,
            "processor": "Intel Core i5-4200U",
            "processorSpeed": 1.6,
            "ram": 4,
            "typeOfStorage": "Spinning HD 5400",
            "storageCapacity": 500
        },
        {
            "id": 3,
            "title": "Acer Aspire V5-573",
            "price": 599.95,
            "operatingSystem": "Windows 8.1",
            "dimensions": "377 x 23 x 255",
            "weight": 2.045,
            "transportWeight": 2.46,
            "processor": "Intel Core i7-4500U",
            "processorSpeed": 1.8,
            "ram": 8,
            "typeOfStorage": "Spinning HD 5400",
            "storageCapacity": 1000
        },
        {
            "id": 4,
            "title": "Acer V5-552",
            "price": 479.99,
            "operatingSystem": "Windows 8.0",
            "dimensions": "378 x 24 x 254",
            "weight": 2,
            "transportWeight": 2.41,
            "processor": "AMD A10-5757M",
            "processorSpeed": 2.1,
            "ram": 6,
            "typeOfStorage": "Spinning HD 5400",
            "storageCapacity": 1000
        },
        {
            "id": 5,
            "title": "Apple MacBook Air 11-inch (2013)",
            "price": 585.00,
            "operatingSystem": "OSX 10.8.4",
            "dimensions": "299 x 13.4 x 192",
            "weight": 1.079,
            "transportWeight": 1.39,
            "processor": "Intel Core i5-4250U",
            "processorSpeed": 1.3,
            "ram": 4,
            "typeOfStorage": "SSD",
            "storageCapacity": 128
        },
        {
            "id": 6,
            "title": "Apple MacBook Air 13-inch (2013)",
            "price": 669.99,
            "operatingSystem": "OSX 10.8.4",
            "dimensions": "325 x 12.6 x 227",
            "weight": 1.328,
            "transportWeight": 1.64,
            "processor": "Intel Core i5-4250U",
            "processorSpeed": 1.3,
            "ram": 4,
            "typeOfStorage": "SSD",
            "storageCapacity": 128
        },
        {
            "id": 7,
            "title": "Apple Macbook Pro Retina 13-inch (2013)",
            "price": 856.39,
            "operatingSystem": "Mac OS 10.9 ",
            "dimensions": "314 x 17.8 x 219",
            "weight": 1.566,
            "transportWeight": 1.94,
            "processor": "Intel Core i5-4258U",
            "processorSpeed": 2.4,
            "ram": 4,
            "typeOfStorage": "SSD",
            "storageCapacity": 128
        },
        {
            "id": 8,
            "title": "Apple Macbook Pro Retina 15-inch (2013)",
            "price": 1338.00,
            "operatingSystem": "Mac OS 10.9",
            "dimensions": "359 x 18 x 247",
            "weight": 1.997,
            "transportWeight": 2.37,
            "processor": "Intel Core i7-4750HQ",
            "processorSpeed": 2,
            "ram": 8,
            "typeOfStorage": "SSD",
            "storageCapacity": 256
        }
    ];

    return {
        get: function(){
            /**
             * Return the data
             */
            return data;
        }
    };
});