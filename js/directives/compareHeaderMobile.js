/**
 * Created by paolo on 07/01/15.
 *
 * This directive create the header for the desktop version
 */

compare.directive('compareHeaderMobile', function(){
    return {
        templateUrl: 'templates/header-mobile.html'
    }
});
