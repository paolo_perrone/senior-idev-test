/**
 * Created by paolo on 07/01/15.
 *
 * This directive create the tech specs for the products
 */

compare.directive('compareTechSpecs', function(){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            product: '=prod'
        },
        templateUrl: 'templates/tech-specs.html'
    }
});