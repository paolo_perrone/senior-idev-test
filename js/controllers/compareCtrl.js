/**
 * Created by paolo on 06/01/15.
 *
 * The main controller of the app
 */

compare.controller('CompareCtrl', function ($scope, $filter, compareModel){
    // Get the data
    var products = $scope.products = compareModel.get();

    // Initialization
    $scope.productsToCompare = products;
    $scope.productsToCompare.unshift({"id": 0, "title": "Select..."});
    $scope.selectedProducts = [$scope.productsToCompare[0], $scope.productsToCompare[0], $scope.productsToCompare[0]];
    $scope.mobileSelectIsHidden = true;
    $scope.currentMobileIndex = 0;
    $scope.currentMobileProduct = $scope.productsToCompare[0];

    // Filters the products. Returns only not selected products.
    $scope.changeSelection = function() {
        $scope.productsToCompare = $filter('filter')(products, function (product, index) {
            return ($scope.selectedProducts[0].id != 0 ? product.id != $scope.selectedProducts[0].id : true) &&
                ($scope.selectedProducts[1].id != 0 ? product.id != $scope.selectedProducts[1].id : true) &&
                ($scope.selectedProducts[2].id != 0 ? product.id != $scope.selectedProducts[2].id : true);
        });
    };

    // Resets a single selection
    $scope.resetSelection = function(index){
        $scope.selectedProducts[index] = $scope.productsToCompare[0];

        $scope.changeSelection();
    };

    // Changes the current selected product
    $scope.changeMobileSelection = function(){
        if($scope.currentMobileProduct.id != 0) {
            $scope.selectedProducts[$scope.currentMobileIndex] = $scope.currentMobileProduct;
        }

        $scope.mobileSelectIsHidden = true;
    };

    // Shows the mobile select
    $scope.showMobileSelect = function(index){
        $scope.mobileSelectIsHidden = false;
        $scope.currentMobileIndex = index;

        $scope.selectedProducts[index] = $scope.productsToCompare[0];

        $scope.changeSelection();
        $scope.currentMobileProduct = $scope.productsToCompare[0];
    };

    // Returns the title of a product
    $scope.currentMobileProductTitle = function(){
        return ($scope.currentMobileIndex + 1) + ". " + ($scope.selectedProducts[$scope.currentMobileIndex].id != 0 ? $scope.selectedProducts[$scope.currentMobileIndex].title :
                "Select a product")
    };

    // Mobile navigation
    $scope.navigateLeft = function(){
        if($scope.currentMobileIndex > 0)
            $scope.currentMobileIndex--;
        else
            $scope.currentMobileIndex = 2;
    };

    $scope.navigateRight = function(){
        if($scope.currentMobileIndex < 2)
            $scope.currentMobileIndex++;
        else
            $scope.currentMobileIndex = 0;
    };
});