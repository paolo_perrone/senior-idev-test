# Syzygy Senior Frontend Developer 

## Result

The comparison tool has been developed using AngularJS and Twitter Bootstrap. Both frameworks are vary good to produce clean e readable code and will be easy upgrade the application and add new features.

Bower has been used to maintain the dependencies.

The application has been tested on IE9+, Chrome, FireFox and on Android/iOS devices.

-----

## Table of Contents
- [Task](#Task)
- [What we're looking for](#what-were-looking-for)
  - [HTML](#html)
  - [CSS](#css)
  - [JavaScript](#javascript)
  - [Thought process](#thought-process)

-----

## Task ##

The task is to produce a simple product comparison tool based on the loose piece of UX supplied with this test.

Applicants can be spend as long or short a time on the test as they see fit. The final result could be in the form of a written spec. An outline of ideas and technologies used etc, however it should be backed up by a working prototype of some or all of the complete comparison tool.

The UX features both a desktop and mobile layout. It is not necessary to complete both if you feel that you have given enough evidence to match the criteria we are looking for as outlined below, but evidence of a thorough understanding of mobile first and responsive design techniques will be noted.

You are free to use any libraries, frameworks and technologies you see fit to meet the brief. If the final output is compiled and production ready (minified etc) then the full source code should also be supplied for evaluation.

We are not looking for backwards compatibility or degradation, and are happy to see prototypes focused on one browser/mobile browser, however please flag in any accompanying notes if this is the case.

-----

## Data ##

| -                     | Acer Aspire E1-572Remove | Acer Aspire S3-392GRemove | Acer Aspire V5-573Remove | Acer V5-552Remove | Apple MacBook Air 11-inch (2013)Remove | Apple MacBook Air 13-inch (2013)Remove | Apple Macbook Pro Retina 13-inch (2013)Remove | Apple Macbook Pro Retina 15-inch (2013) |
|-----------------------|--------------------------|---------------------------|--------------------------|-------------------|----------------------------------------|----------------------------------------|-----------------------------------------------|-----------------------------------------|
| Price                 | �399.00                  | �899.99                   | �599.95                  | �479.99           | �585.00                                | �669.99                                | �856.39                                       | �1,338.00                               |
| Operating system      | Windows 8.1              | Windows 8.1               | Windows 8.1              | Windows 8.0       | OSX 10.8.4                             | OSX 10.8.4                             | Mac OS 10.9                                   | Mac OS 10.9                             |
| Dimensions (mm)       | 380 x 27 x 255           | 221 x 17 x 124            | 377 x 23 x 255           | 378 x 24 x 254    | 299 x 13.4 x 192                       | 325 x 12.6 x 227                       | 314 x 17.8 x 219                              | 359 x 18 x 247                          |
| Weight (kg)           | 2.193                    | 1.618                     | 2.045                    | 2                 | 1.079                                  | 1.328                                  | 1.566                                         | 1.997                                   |
| Transport weight (kg) | 2.47                     | 1.97                      | 2.46                     | 2.41              | 1.39                                   | 1.64                                   | 1.94                                          | 2.37                                    |
| Processor             | Intel Core i5-4200U      | Intel Core i5-4200U       | Intel Core i7-4500U      | AMD A10-5757M     | Intel Core i5-4250U                    | Intel Core i5-4250U                    | Intel Core i5-4258U                           | Intel Core i7-4750HQ                    |
| Processor speed (GHz) | 1.6                      | 1.6                       | 1.8                      | 2.1               | 1.3                                    | 1.3                                    | 2.4                                           | 2                                       |
| Ram (GB)              | 4                        | 4                         | 8                        | 6                 | 4                                      | 4                                      | 4                                             | 8                                       |
| Type of storage       | Spinning HD 5400         | Spinning HD 5400          | Spinning HD 5400         | Spinning HD 5400  | SSD                                    | SSD                                    | SSD                                           | SSD                                     |
| Storage capacity (GB) | 500                      | 500                       | 1000                     | 1000              | 128                                    | 128                                    | 128                                           | 256                                     |

-----

## Wireframe ##
![wireframe](https://bitbucket.org/syzygy-idev/senior-idev-test/raw/develop/support/wireframe.png)

-----

## What we'll be looking for

We'll be looking for evidence of a well constructed approach to solving the problem, clear and legible annotation, as well as some hard evidence of hands on technical coding ability in both javascript, css and html.

### HTML
* Markup conveys good semantic intent
* Accessible structure
* Room for i18n

### CSS
* Namespacing!
* Lean selectors with as little qualification as possible
* Pre-processors are okay

### JavaScript
* Good overall understanding of the language
* **DRY**
* Appropriate use of design patterns
* Loose coupling (within reason)
* Measured opinion - imagine this will be used within the context of a larger platform

### Process
* Commit often
* Automated processes
* Mocking of blockers
* Emphasis on long-term maintainability
* Good Git workflow
